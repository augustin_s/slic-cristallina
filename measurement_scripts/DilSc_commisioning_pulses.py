from time import sleep, time
from cristallina import cta, daq


def make_pulse_train():
    recording = daq.acquire("pulsing_sequence", n_pulses=100 * 101, wait=False)
    cta.start()
    recording.wait()
    print("done")


# daq.acquire(filename, data_base_dir=None, detectors=None, channels=None, pvs=None, scan_info=None, n_pulses=100, continuous=False, is_scan_step=False, wait=True):
