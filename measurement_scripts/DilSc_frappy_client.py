import sys
sys.path.append('/sf/cristallina/applications/frappy')
from frappy.client import SecopClient
from frappy.core import BUSY, IDLE, ERROR, Parameter, Command, Done

# Connect to the dilsc
client = SecopClient('dilsc:5000')
client.connect()

def list_identifiers(client=client):
    return client.identifier

# Set lower ramp rate in z-axis
client.setParameter('mfz','ramp',0.025)



def change_temperature(value,temp='T_pucksensor',client=client):
