
from epics import PV

def pp_normal(opening = True):
    """ Set pulse picker to fixed open or closed mode.
    """
    
    PV("SARES30-LTIM01-EVR0:RearUniv0-Ena-SP").put("Disabled")
    
    PV("SARES30-LTIM01-EVR0:RearUniv0_SOURCE").put("HIGH")
    PV("SARES30-LTIM01-EVR0:RearUniv0_SOURCE2").put("HIGH")
    
    if opening:
        PV("SARES30-LTIM01-EVR0:RearUniv0-Ena-SP").put("Enabled")
    else:
        PV("SARES30-LTIM01-EVR0:RearUniv0-Ena-SP").put("Disabled")
    
def pp_cta():
    """ Set pulse picker to use the CTA as control input. 
        (check connections and parameters!)
    """
    
    PV("SARES30-LTIM01-EVR0:RearUniv0-Ena-SP").put("Disabled")
    PV("SARES30-LTIM01-EVR0:RearUniv0_SOURCE").put("PULSER")
    PV("SARES30-LTIM01-EVR0:RearUniv0_SOURCE2").put("PULSER")
    PV("SARES30-LTIM01-EVR0:RearUniv0_SNUMPD").put("3") 	 
    PV("SARES30-LTIM01-EVR0:RearUniv0_SNUMPD2").put("3")
    PV("SARES30-LTIM01-EVR0:RearUniv0-Ena-SP").put("Enabled")

    
    
    
    
    

