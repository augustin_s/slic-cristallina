from slic.core.device.simpledevice import SimpleDevice
from slic.devices.general.motor import Motor

standa_z = Motor("SARES30-MOBI1:MOT_1")
standa_x = Motor("SARES30-MOBI1:MOT_2")

standa = SimpleDevice("Standa X-ray eye base motors", x=standa_x, z=standa_z)
