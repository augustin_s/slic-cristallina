# from slic.devices.general.smaract import SmarActStage
from devices.smaract import SmarActStage
from slic.core.device.simpledevice import SimpleDevice
from slic.devices.general.motor import Motor


# this currently uses a modified SmarActStage module
# otherwise the wait times are not working correctly.

smaract_z = Motor("SARES30-MCS2750:MOT_1")
smaract_x = Motor("SARES30-MCS2750:MOT_2")
smaract_y = Motor("SARES30-MCS2750:MOT_3")


smaract_mini_XYZ = SimpleDevice("XYZ smaract stage (mini)", x=smaract_x, y=smaract_y, z=smaract_z)


smaract_Juraj = SmarActStage(
    "SARES30-XSMA156",
    X="SARES30-XSMA156:X",
    Y="SARES30-XSMA156:Y",
    Z="SARES30-XSMA156:Z",
    Ry="SARES30-XSMA156:Ry",
    Rx="SARES30-XSMA156:Rx",
    Rz="SARES30-XSMA156:Rz",
)

# smaract_mini_XYZ = SmarActStage("SARES30-MCS2750",
#                        X='SARES30-MCS2750:MOT_1',
#                        Y='SARES30-MCS2750:MOT_2',
#                        Z='SARES30-MCS2750:MOT_3',
#                        )
