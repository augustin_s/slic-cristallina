from devices.attocube import AttocubeStage

attocube = AttocubeStage(
    "SARES30-ATTOCUBE",
    Y="SARES30-ATTOCUBE:A2",
    X="SARES30-ATTOCUBE:A1",
)

# def make_AttocubeStage(name):
# 	return AttocubeStage(name,
# 			Z=name+':A0',
# 			X=name+':A1',
# 			)

# class NamedAttocubeStage(AttocubeStage):
# 	def __init__(self, name):
# 		super().__init__(name,
# 			Z=name+':A0',
# 			X=name+':A1',
# 		)


# def set_target_value(self, value):
# 	t1 = self.stage1.set_target_value(value)
# 	t2 = self.stage2.set_target_value(-value)
# 	t1.wait()
# 	t2.wait()

# def is_moving(self):
# 	return any(self.stage1.is_moving(), self.stage2.is_moving())


# t = daq.aquire(...)
# for pos in []:
# 	mot.set_target_value(pos).wait()
# 	sleep()
# t.stop()
