from slic.core.device import Device, SimpleDevice
from slic.devices.general.motor import Motor


class SlitUnitCenterWidthJJ(Device):
    def __init__(self, ID, motor_naming="MOTOR", **kwargs):
        super().__init__(ID, **kwargs)

        self.x = SimpleDevice(
            ID + "-X", center=Motor(ID + ":" + motor_naming + "_X"), width=Motor(ID + ":" + motor_naming + "_W")
        )

        self.y = SimpleDevice(
            ID + "-Y", center=Motor(ID + ":" + motor_naming + "_Y"), width=Motor(ID + ":" + motor_naming + "_H")
        )


jjslits = SlitUnitCenterWidthJJ("SARES30-MOBI2", motor_naming="MOT")
