from slic.core.device.simpledevice import SimpleDevice
from slic.devices.general.motor import Motor

newport_z = Motor("SARES30-MOBI1:MOT_5")

newport = SimpleDevice("Newport linear stage base motors", z=newport_z)
