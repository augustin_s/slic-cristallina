from time import sleep
from epics import PV
#from slic.devices.xoptics.aramis_attenuator import Attenuator

pulse_picker_opening_pv = "SARES30-LTIM01-EVR0:RearUniv0-Ena-SP"

def attenuator_scan(transmissions,att,daq,sleep_time=8,label='test',n_pulses=100,detectors=None,pvs=None,channels=None):
    
    PV(pulse_picker_opening_pv).put("Disabled")
    sleep(1)

    for idx,transmission in enumerate(transmissions):
        
        print("Setting transmission = ",transmission)
        att.trans1st(transmission)
        sleep(sleep_time)

        PV(pulse_picker_opening_pv).put("Enabled")
        sleep(1)
	
        print("Acquiring scan point ",idx,", Transmission = ",transmission)
	
        if idx == 0:
            #print("First point")
            daq.acquire(label,is_scan_step=False,n_pulses=n_pulses,detectors=detectors,channels=channels,pvs=pvs)

        else:
            #print("Not first point")
            daq.acquire(label,is_scan_step=True,n_pulses=n_pulses,detectors=detectors,channels=channels,pvs=pvs)
	    
        PV(pulse_picker_opening_pv).put("Disabled")
        sleep(1)

	
    print("Returning to first transmission = ",transmissions[0])
    att.trans1st(transmissions[0])
    sleep(sleep_time)
        


def attenuator_scan_cont(transmissions,att,daq,sleep_time=8,label='test',n_pulses=100,detectors=None,pvs=None,channels=None):
    
    PV(pulse_picker_opening_pv).put("Disabled")
    sleep(1)

    for idx,transmission in enumerate(transmissions):
        
        print("Setting transmission = ",transmission)
        att.trans1st(transmission)
        sleep(sleep_time)

        PV(pulse_picker_opening_pv).put("Enabled")
        sleep(1)
	
        print("Acquiring scan point ",idx,", Transmission = ",transmission)
	
        if idx < 0:
            #print("First point")
            daq.acquire(label,is_scan_step=False,n_pulses=n_pulses,detectors=detectors,channels=channels,pvs=pvs)

        else:
            #print("Not first point")
            daq.acquire(label,is_scan_step=True,n_pulses=n_pulses,detectors=detectors,channels=channels,pvs=pvs)
	    
        PV(pulse_picker_opening_pv).put("Disabled")
        sleep(1)

	
    print("Returning to first transmission = ",transmissions[0])
    att.trans1st(transmissions[0])
    sleep(sleep_time)
        

